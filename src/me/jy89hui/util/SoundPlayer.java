package me.jy89hui.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import me.jy89hui.Music.UNIVERSAL;

public class SoundPlayer { 
 private class PlayFile implements Runnable { 
  private String strFilename; 
   
  public void run() { 
   System.out.println("SoundPlayer.playFile: Attempting to play: "+ strFilename); 
   if (System.getProperty("os.name").equals("Linux")) {
    Boolean extPlay = true; 
    try { 
     Runtime.getRuntime().exec("aplay "+strFilename); 
    } catch (Exception e) { 
     e.printStackTrace(); 
     extPlay = false; 
    } 
     
    // If External Play was successful, then we continue 
    if (extPlay) { 
     System.out.println("Played file successfully via aplay"); 
     return; 
    } 
   } 
   /*
     Now, that we're sure there is an argument, we 
     take it as the filename of the soundfile 
     we want to play. 
   */ 
   //String strFilename = args[0]; 
   File soundFile = new File(strFilename); 
   
   /*
     We have to read in the sound file. 
   */ 
   AudioInputStream audioInputStream = null; 
  // AudioFormat audioFormat = null;
   try { 
	   audioInputStream = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, AudioSystem.getAudioInputStream(soundFile));
	   //audioFormat=  audioInputStream.getFormat();
   } 
   catch (Exception e) { 
    /*
      In case of an exception, we dump the exception 
      including the stack trace to the console output. 
    */ 
    e.printStackTrace(); 
    return; 
   } 
   PlayStream stream = new PlayStream(); 
   stream.audioInputStream = audioInputStream; 
   stream.run(); 
  } 
 } 
  
 class PlayStream implements Runnable { 
	 //128000
  private static final int EXTERNAL_BUFFER_SIZE = 1024; 
  private AudioInputStream audioInputStream; 
    
  public void run() { 
   AudioFormat audioFormat = audioInputStream.getFormat(); 
 
   SourceDataLine line = null; 
   DataLine.Info info = new DataLine.Info(SourceDataLine.class, 
              audioFormat); 
   try { 
    line = (SourceDataLine) AudioSystem.getLine(info); 
    /*
      The line is there, but it is not yet ready to 
      receive audio data. We have to open the line. 
      Why though... I dont know.
    */ 
    line.open(audioFormat); 
   } 
   catch (LineUnavailableException e) { 
    e.printStackTrace(); 
   } 
   catch (Exception e) { 
    e.printStackTrace(); 
   } 
   line.start(); 
  
   int nBytesRead = 0; 
   byte[] abData = new byte[EXTERNAL_BUFFER_SIZE]; 
   while (nBytesRead != -1) { 
    try { 
     nBytesRead = audioInputStream.read(abData, 0, abData.length); 
    } 
    catch (IOException e) 
    { 
     e.printStackTrace(); 
    } 
    if (nBytesRead >= 0) { 
    	FloatControl control = (FloatControl)line.getControl(FloatControl.Type.MASTER_GAIN);
        control.setValue(limit(control,UNIVERSAL.volume));
        
     //int nBytesWritten =  
     line.write(abData, 0, nBytesRead); 
     String s = "";
     for (byte b : abData){
    	 s+=b+",";
     }
     System.out.println(s);
     System.out.println(abData.toString()+" "+nBytesRead);
    } 
   } 
  
   line.drain(); 
   line.close(); 
 
  } 
   
 } 
  
 class QueueRunner extends Thread { 
  public void run() { 
    
   setName("SoundPlayer: QueueRunner"); 
    
   while (runner != null) { 
    try { 
     Runnable entry = queue.poll(60, TimeUnit.SECONDS); 
     if (entry == null) { 
      synchronized(SoundPlayer.this) { 
       if(queue.isEmpty()) 
        runner = null; 
      } 
     } else { 
      entry.run(); 
     } 
    } catch (InterruptedException e) { 
     runner = null; 
    } 
   } 
  } 
 } 
  
 protected static SoundPlayer instance = null; 
 private BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(20);; 
 private QueueRunner runner; 
  
 synchronized private static SoundPlayer getInstance(){ 
  if (instance == null) { 
   instance = new SoundPlayer(); 
  } 
  return instance; 
 } 
  
 public static void play(AudioInputStream audioStream) { 
  getInstance().playStream(audioStream); 
 } 
  
 public static void play(InputStream stream) { 
  AudioInputStream audioInputStream = null; 
  try { 
   audioInputStream = AudioSystem.getAudioInputStream(stream); 
   getInstance().playStream(audioInputStream); 
  } 
  catch (Exception e) { 
   e.printStackTrace(); 
  } 
 } 
   
 public static void play(String strFilename){ 
  getInstance().playFile(strFilename); 
 } 
  
 protected void playSound(Runnable entry) { 
  synchronized(this) { 
   if (!queue.offer(entry)) { 
    System.err.println("Sound queue full, not playing sound."); 
   } 
   if(runner == null) { 
    runner = new QueueRunner(); 
    runner.start(); 
   } 
  } 
 } 
  
 private void playFile(String strFilename) { 
  PlayFile player = new PlayFile(); 
  player.strFilename = strFilename; 
  playSound(player); 
 } 
  
 private void playStream(AudioInputStream audioStream) { 
  PlayStream player = new PlayStream(); 
  player.audioInputStream = audioStream; 
  playSound(player); 
 } 


 private static float limit(FloatControl control,float level)
 { return Math.min(control.getMaximum(), Math.max(control.getMinimum(), level)); }
}