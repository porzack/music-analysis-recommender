package me.jy89hui.util;

import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;
import me.jy89hui.Player.Main;

public class SoundJLayer extends PlaybackListener implements Runnable
{
    private String filePath;
    private AdvancedPlayer player;
    private Thread playerThread;    

    public SoundJLayer(String filePath)
    {
        this.filePath = filePath;
    }
    
    public void play()
    {
        try
        {
            String urlAsString = 
                "file:///" 
                + this.filePath;

            this.player = new AdvancedPlayer
            (
                new java.net.URL(urlAsString).openStream(),
                javazoom.jl.player.FactoryRegistry.systemRegistry().createAudioDevice()
            );

            this.player.setPlayBackListener(this);

            this.playerThread = new Thread(this, "AudioPlayerThread");

            this.playerThread.start();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // PlaybackListener members
    @Override
    public void playbackStarted(PlaybackEvent playbackEvent)
    {
    	Main.hasStartedPlayingASong(this);
    }
    @Override
    public void playbackFinished(PlaybackEvent playbackEvent)
    {
        Main.hasStoppedPlayingSong();
    }   
    public void stop(){
    	this.player.stop();
    }

    // Runnable members
    
    public void run()
    {
        try	
        {	
            this.player.play();
        }
        catch (javazoom.jl.decoder.JavaLayerException ex)
        {
            ex.printStackTrace();
        }

    }
}