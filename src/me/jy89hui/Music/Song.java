package me.jy89hui.Music;

import java.util.HashMap;

import me.jy89hui.Attributes.SoundAttribute;
import me.jy89hui.util.SoundJLayer;

public class Song {
	public String songPath= "";
	public String beatPatternPath = "";
	public String matchabilityPath = "";
	public HashMap<SoundAttribute, Integer> attributes;
	private SoundJLayer sound;
	
	public Song(String filePath, HashMap<SoundAttribute, Integer> attributes){
		this.songPath=filePath;
		this.attributes=attributes;
		sound = new SoundJLayer(this.songPath+".mp3");
	}
	public void play(){
        sound.play();
	}
	public void stop(){
		sound.stop();
	}
}
