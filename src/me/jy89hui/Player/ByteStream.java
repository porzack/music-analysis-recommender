package me.jy89hui.Player;

import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

import me.jy89hui.Music.UNIVERSAL;

public class ByteStream implements Runnable{
	private int chunkSize;
	private Source source;
	private boolean playSound;
	/**
	 * HOW TO USE:
	 * 
	 * 		
	   ByteStream bs = new ByteStream(s,[NUMBER_OF_BYTES],true){
			public void go(byte[] bytes){
			}
			public void done(){
			}
		};
		bs.run();
	 * @param source
	 * @param chunkSize
	 * @param playSound
	 */
	public ByteStream(Source source, int chunkSize, boolean playSound){
		this.source=source;
		this.chunkSize=chunkSize;
		this.playSound=playSound;
	}
	public void go(byte[] bytes){
		
	}
	public void done(){
		
	}
	@Override
	public void run(){
		   AudioInputStream audioInputStream = null;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, AudioSystem.getAudioInputStream(source.getFile()));
		} catch (UnsupportedAudioFileException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}; 

		   AudioFormat audioFormat = audioInputStream.getFormat(); 
		 
		   SourceDataLine line = null; 
		   DataLine.Info info = new DataLine.Info(SourceDataLine.class, 
		              audioFormat); 
		   try {
		    line = (SourceDataLine) AudioSystem.getLine(info); 
		    /*
		      The line is there, but it is not yet ready to 
		      receive audio data. We have to open the line. 
		      Why though... I dont know.
		    */ 
		    line.open(audioFormat); 
		   } 
		   catch (LineUnavailableException e) {
		    e.printStackTrace(); 
		   } 
		   catch (Exception e) {
		    e.printStackTrace(); 
		   } 
		   line.start(); 
		  
		   int nBytesRead = 0; 
		   byte[] abData = new byte[chunkSize]; 
		   while (nBytesRead != -1) { 
		    try { 
		     nBytesRead = audioInputStream.read(abData, 0, abData.length); 
		    } 
		    catch (IOException e) 
		    { 
		     e.printStackTrace(); 
		    } 
		    if (nBytesRead >= 0) { 
		    	FloatControl control = (FloatControl)line.getControl(FloatControl.Type.MASTER_GAIN);
		        control.setValue(limit(control,UNIVERSAL.volume)); 
		    if (playSound){ line.write(abData, 0, nBytesRead);} 
		    this.go(abData);
		    } 
		   } 
		  
		   line.drain(); 
		   line.close(); 
		   this.done();
		  } 
		    
		
	private static float limit(FloatControl control,float level)
	 { return Math.min(control.getMaximum(), Math.max(control.getMinimum(), level)); }
	}
	
	

