package me.jy89hui.Player;

import java.io.File;

public class Source {
	private String loc;
	private long startByte;
	private long endByte;
	public Source(String filelocation){
		this(filelocation, 0, -1);
	}
	public Source(String fileLocation, long startByte, long endByte){
		//if (!fileLocation.endsWith(".mp3")){
			//throw new RuntimeException("Tried to create an invalid Source : "+fileLocation);
		//}
		this.startByte=startByte;
		this.endByte=endByte;
		loc=fileLocation;
	}
	public File getFile(){
		return new File(loc);
	}
	public String getDirectLoc(){
		return loc;
	}
	public String getFileName(){
		String[] secs = loc.split("/");
		String[] parts = secs[secs.length-1].split(".mp3");
		return parts[0];
	}
	
}
