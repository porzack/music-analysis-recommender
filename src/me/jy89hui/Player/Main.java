package me.jy89hui.Player;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import jAudioFeatureExtractor.DataModel;
import jAudioFeatureExtractor.ModelListener;
import jAudioFeatureExtractor.AudioFeatures.FeatureExtractor;
import jAudioFeatureExtractor.DataTypes.RecordingInfo;
import jAudioFeatureExtractor.jAudioTools.AudioSamples;
import me.jy89hui.Music.UNIVERSAL;
import me.jy89hui.graphicalInterface.MusicalGraph;
import me.jy89hui.util.SoundJLayer;
import me.jy89hui.util.SoundPlayer;
import me.jy89hui.util.logger;

public class Main {
	/*
	 * TODO List:
	 * 
	 * - Create way of storing songs
	 * - Create way of storing beat patters
	 * - Create way of mixing songs
	 * - Create AI to determine my mood
	 * - Create way of determining a songs genre
	 * - ENJOY
	 * 
	 */
	public static boolean playingASong=false;
	public static final String BASE_DIRECTORY = "/Users/Zack/Desktop/Music/";
	public static void main(String[] args) throws Exception{
		String prefsloc = "/Users/Zack/Desktop/Tools/Eclipse/eclipse_Java/Libs/jAudio-1.0.4/jAudio/jAudio/features.xml";
		Source s = new Source("/Users/Zack/Desktop/Music/test.mp3");
		//Source s3= new Source("/Users/Zack/Desktop/Music/LRT.txt");
		logger.log(""+s.getFileName()+" "+Byte.MAX_VALUE);
		MusicalGraph.test();
		//DataModel dm = new DataModel(prefsloc,null);
		//RecordingInfo[] recordingInfo = new RecordingInfo[1];
		//[0] = new RecordingInfo(s3.getDirectLoc(),s.getDirectLoc(),new AudioSamples(s.getFile(), s3.getDirectLoc(),false), false);
		//dm.extract(4096, 0.5D, 0.5, true, false, true, recordingInfo, 2);
	//	dm.x
		
	}
	
	
	public static void hasStartedPlayingASong(SoundJLayer song){
		if (playingASong){
			throw new RuntimeException("Tried to start 2 songs at once.");
		}else{
			playingASong=true;
		}
	}
	public static void hasStoppedPlayingSong(){
		if (!playingASong){
			throw new RuntimeException("Tried to stop when the song had not started");
		}else{
			playingASong=false;
		}
	}
	/*
	 * How this program is going to work:
	 * 
	 * Give it like fucking tons of music. 
	 * 
	 * Have it analize this music and pick out smaller sections that all sound similar
	 *
	 * Go through entire .mp3 while comparing it to the smaller sections you have selected. Now determine what % of the .mp3 falls under each section
	 * 
	 * Go through .mp3s and select .mp3s that have a high % here or there. 
	 * 
	 * When I am in the need for music, begin playing this music and selecting .mp3s (using AI) progressivly getting better
	 * - When it is sure how I am feeling, ask me how I would classify my feelings. 
	 * 
	 * Now go label those high %s under the feelings that I have just listed. 
	 * 
	 * Over time good music will arise. 
	 * 
	 */
	
}
