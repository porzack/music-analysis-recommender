package me.jy89hui.graphicalInterface;

import java.io.FileFilter;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Path;

import com.sun.media.sound.FFT;

import me.jy89hui.Player.ByteStream;
import me.jy89hui.Player.Source;
import me.jy89hui.util.logger;

public class MusicalGraph  extends BasicGame{
	private static final int WIDTH=1800;
	private static final int HEIGHT=1100;
	private int mpx; // mouseposx
	private int mpy; // mouseposy
	private ArrayList<Byte[]> lines = new ArrayList<Byte[]>();
	private MusicalGraph(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public static MusicalGraph create(String name) throws SlickException{
		MusicalGraph mg = new MusicalGraph(name);
		AppGameContainer app = new AppGameContainer(mg);
		app.setDisplayMode(WIDTH, HEIGHT, false);
		app.setAlwaysRender(true);
		app.setShowFPS(true);
		new Thread(){
			public void run(){
				try {
					app.start();
				} catch (SlickException e) {
					e.printStackTrace();
				}
			}
		}.start();
		return mg;
		
	}
	public void addLine(byte[] line){
		Byte[] line1 = new Byte[line.length];

		int i=0;    
		for(byte b: line){
		   line1[i++] = b; 
		}
		
		// ^ Transforming byte[] to Byte[]
		lines.add(line1);
	}
	public void addLine(Byte[] line){
		lines.add(line);
	}
	public void clearLines(){
		lines = new ArrayList<Byte[]>();
	}

	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		g.setBackground(Color.white);
		try{
		if (lines.size()<1){
			return;
		}
		int longestline=0;
		for (Byte[] b : lines){
			if (b.length>longestline){
				longestline=b.length;
			}
		}
		if (longestline==0){
			return;
		}
		// Byte.Max_Val is the max for a signed byte.
		int imaginaryHeight= Byte.MAX_VALUE*2;
		int imaginaryWidth = longestline;
		float heightScale = (float)HEIGHT/imaginaryHeight;
		float widthScale = (float)WIDTH/imaginaryWidth;
		int l=0;
		for (Byte[] line : lines){
			Path path = new Path(0,line[0]*heightScale+(HEIGHT/2));
			for (int x =0; x<line.length; x++){
				path.lineTo(x*widthScale, HEIGHT-((line[x]+127)*heightScale));
			}
			g.setColor(this.getColor(l));
			g.draw(path);
			l++;
		}
		g.setColor(new Color(30,255,20));
		g.drawLine(0, HEIGHT-(0*heightScale), WIDTH, HEIGHT-(255*heightScale));
		this.drawMouseThing(g);
		}catch(Exception e){
			// Because screw it.
		}
	}
	private Color getColor(int line){
		if (line==0){
			return new Color(0,0,0);
		}
		else if (line==1){
			return new Color(250,20,20);
		}
		else if (line==2){
			return new Color(20,250,20);
		}
		else if (line==3){
			return new Color(20,20,250);
		}
		else if (line==4){
			return new Color(230,230,20);
		}
		else if (line==5){
			return new Color(250,20,250);
		}
		else if (line==6){
			return new Color(20,250,250);
		}
		else if (line==7){
			return new Color(192,192,192);
		}
		else if (line==8){
			return new Color(128,0,0);
		}
		else if (line==9){
			return new Color(0,128,0);
		}
		else if (line==10){
			return new Color(0,0,128);
		}
		else if (line==11){
			return new Color(128,128,0);
		}
		else if (line==12){
			return new Color(128,0,128);
		}
		else if (line==13){
			return new Color(0,128,128);
		}
		else if (line==14){
			return new Color(128,128,128);
		}
		else if (line==15){
			return new Color(100,149,237);
		}
		return new Color(255,255,255);
	}
	private void drawMouseThing(Graphics g){
		g.setColor(new Color(250,20,20));
		int posx = mpx;
		int posy = mpy;
		int imaginaryHeight= Byte.MAX_VALUE*2;
		float heightScale = (float)HEIGHT/imaginaryHeight;
		int vol = (int)(((HEIGHT-posy)/heightScale - 127));
		g.drawString(""+vol, posx, posy-15);
		g.setColor(new Color(255,255,255));
	}
	@Override
	public void init(GameContainer arg0) throws SlickException {
		
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
	}
	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy){
		mpx=newx;
		mpy=newy;
	}
	public static void test() throws SlickException{
		Source s = new Source("/Users/Zack/Desktop/test.mp3");
		MusicalGraph mg = MusicalGraph.create("LRTEST");
		ByteStream bs = new ByteStream(s,(int)Math.pow(2,10),true){
			List<Byte> current = new ArrayList<Byte>();
			public void go(byte[] bytes){
				long combined = 0;
				byte[] mono = new byte[bytes.length/2];
				 for (int i = 0 ; i < mono.length/2; ++i){
					 int HI = 0; int LO = 1;
					 int left = (bytes[i * 4 + HI] << 8) | (bytes[i * 4 + LO] & 0xff);
					 int right = (bytes[i * 4 + 2 + HI] << 8) | (bytes[i * 4 + 2 + LO] & 0xff);
					 int avg = (left + right) / 2;
					 mono[i * 2 + HI] = (byte) avg;//(byte)((avg >> 8) & 0xff);
					 mono[i * 2 + LO] = (byte)avg;//(byte)(avg & 0xff);
					 
		            }
				 for (byte b : mono){
					 combined+=b;
				 }
				combined /= mono.length;
				if(combined != 0){
				}
				current.add((byte)combined);
				Byte[] bytearray = new Byte[current.size()];
				int x=0;
				for (Byte pos : current){
					bytearray[x++]=pos;
				}
				mg.clearLines();
				mg.addLine(bytearray);
				if(current.size()>1600){
					current.remove(0);
				}
			}
			public void done(){
				Byte[] bytearray = new Byte[current.size()];
				int x=0;
				for (Byte pos : current){
					bytearray[x++]=pos;
				}
				mg.addLine(bytearray);
			}
		};
		bs.run();
	}
	
	

}
