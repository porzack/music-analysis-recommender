package me.jy89hui.Attributes;

import me.jy89hui.util.logger;

public enum SoundAttribute {
	/*
	 * HOW IDS ARE ORGANIZED:
	 * 100= FEELINGS 
	 * 200= BASIC GENRES
	 */
	DEEP (101),
	SUPER_JUMPY (102),
	IN_FUCKING_LOVE(103),
	BEAT_SOME_SHIT (104),
	HIGH_VOCAL(105),
	LOW_VOCAL (106), 
	HAPPY (107), 
	PUMP_ME_UP (108),
	SLOW (109),
	LOVEY (110),
	INSIPIRING (111),
	MOZART_SHIT(112),
	NOBODY_IS_HOME_TURN_ON_THE_FUCKING_WHATEVER(113),
 
	HEADACHE_FUCKING_BASS (201), 
	CHILL (202), 
	GIMME_SOME_GLITCH (203), 
	COUNTRY (204),
	EDM (205),
	CHILL_LOVE (206),
	FUCKING_OMFG (207), 
	JAZZY (208), 
	DUBSTEP (209),
	TRAP (210),
	NIGHTCORE(211),
	METAL (212),
	RAP(213),
	CLUB(214),
	HOUSE (215),
	ELECTRO_HOUSE (216),
	EXERCISE (217),
	POP (218),
	
	
	
	
	
	;
	public int ID;
	SoundAttribute(int ID){
		this.ID=ID;
	}
	public String toString(){
		return "{"+this.name()+","+this.ID+"}";
	}
	public static SoundAttribute getAttribute(int ID){
		for (SoundAttribute sa : SoundAttribute.values()){
			if (sa.ID==ID){
				return sa;
			}
		}
		logger.log("Error. SoundAttribute ID:"+ID+" Does not exist. Has it been removed?");
		return null;
	}
}
